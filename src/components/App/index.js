import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Navigation from '../Navigation';
import LandingPage from '../Landing';
import SignUpPage from '../SignUp';
import SignInPage from '../SignIn';
import PasswordForgetPage from '../PasswordForget';
import HomePage from '../Home';
import AccountPage from '../Account';
import AdminPage from '../Admin';
import Freelancer from '../Freelancer';
import Business from '../Business';

import * as ROUTES from '../../constants/routes';
import { withAuthentication } from '../Session';

const App = () => (
  <Router>
  <div>
    <header className="header">
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container"> <a className="navbar-brand" href="index.html"><img src="images/logo.png" alt="" /></a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation"> <span className="navbar-toggler-icon"></span> </button>
          <div className="collapse navbar-collapse justify-content-end" id="navbarTogglerDemo03">
            <Navigation/>
            
          </div>
        </div>
      </nav>
    </header>
   
      <Route exact path={ROUTES.LANDING} component={LandingPage} />
      <Route path={ROUTES.SIGN_UP} component={SignUpPage} />
      <Route path={ROUTES.SIGN_IN} component={SignInPage} />
      <Route
        path={ROUTES.PASSWORD_FORGET}
        component={PasswordForgetPage}
      />
      <Route path={ROUTES.HOME} component={HomePage} />
      <Route path={ROUTES.ACCOUNT} component={AccountPage} />
      <Route path={ROUTES.ADMIN} component={AdminPage} />
      <Route path={ROUTES.FREELANCER} component={Business} />
      <Route path={ROUTES.BUSINESS} component={Freelancer} />
    
   <footer className="footer">
    <img src="images/footer-hero.png" alt="" className="img-fluid"/>
    <div className="footer-hero bg-primary pt-3 pb-5">
      <div className="container">
        <div className="row justify-content-md-center">
          <div className="col-lg-6 col-md-8">
            <ul className="list-unstyled text-white">
              <li><img src="images/ico1.png" alt="" className="mr-2" /> Work with best talents</li>
              <li><img src="images/ico2.png" alt="" className="mr-2" /> Get Corporatised services</li>
              <li><img src="images/ico3.png" alt="" className="mr-2" /> Stay Budget Friendly</li>
            </ul>
          </div>
          <div className="col-lg-2 col-md-4 align-self-center"> <a href="#" className="btn btn-warning btn-lg">Get Started</a> </div>
        </div>
      </div>
    </div>
    <div className="container">
      <div className="footer-main row">
        <div className="col-md-3 mb-3">
          <h6>bonhub</h6>
          <ul>
            <li><a href="#">About Us</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">FAQ</a></li>
            <li><a href="#">Contact Us</a></li>
          </ul>
        </div>
        <div className="col-md-3 mb-3">
          <h6>Our Services</h6>
          <ul>
            <li><a href="#">Freelancer</a></li>
            <li><a href="#">Businesses</a></li>
          </ul>
        </div>
        <div className="col-md-6">
          <h5>Stay Connected</h5>
          <p>Our Growth Marketers write our blogs with care.<br/>
            Stay tuned with the latest in Digital Marketing.</p>
          <form>
            <div className="form-group row">
              <div className="col-md-8">
                <input type="text" className="form-control" placeholder="Your email here" />
              </div>
              <div className="col-md-4">
                <button type="button" className="btn btn-block btn-warning buttonRed " >Subscribe</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div className="footer-copyright">
        <div className="row no-gutters">
          <div className="col-md-6">bonhub © 2019 - All Rights Reserved</div>
          <div className="col-md-6 text-right"> <a href="#">Disclaimer</a> / <a href="#">Privacy Policy</a>
            <nav className="nav d-inline-block"> <a className="nav-link" href="#"><i className="fab fa-facebook-f"></i></a> <a className="nav-link" href="#"><i className="fab fa-twitter"></i></a> <a className="nav-link" href="#"><i className="fab fa-instagram"></i></a> <a className="nav-link" href="#"><i className="fab fa-linkedin-in"></i></a> </nav>
          </div>
        </div>
      </div>
    </div>
  </footer>
  </div>
  </Router>
);

export default withAuthentication(App);
