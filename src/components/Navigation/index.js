import React from 'react';
import { Link } from 'react-router-dom';

import { AuthUserContext } from '../Session';
import SignOutButton from '../SignOut';
import * as ROUTES from '../../constants/routes';
import * as ROLES from '../../constants/roles';

const Navigation = () => (
  <AuthUserContext.Consumer>
    {authUser =>
      authUser ? (
        <NavigationAuth authUser={authUser} />
      ) : (
        <NavigationNonAuth />
      )
    }
  </AuthUserContext.Consumer>
);

const NavigationAuth = ({ authUser }) => (
  <ul>
    <li>
      <Link to={ROUTES.LANDING}>Landing</Link>
    </li>
    <li>
      <Link to={ROUTES.HOME}>Home</Link>
    </li>
    <li>
      <Link to={ROUTES.ACCOUNT}>Account</Link>
    </li>
    {authUser.roles.includes(ROLES.ADMIN) && (
      <li>
        <Link to={ROUTES.ADMIN}>Admin</Link>
      </li>
    )}
    <li>
      <SignOutButton />
    </li>
  </ul>
);

const NavigationNonAuth = () => (
  <ul className="navbar-nav mt-2 mt-lg-0">
    
    <li className="nav-item">
      <Link className="nav-link" to={ROUTES.FREELANCER}>Freelancer</Link>
    </li>
    <li className="nav-item">
      <Link className="nav-link" to={ROUTES.BUSINESS}>Businesses</Link>
    </li>
    <li className="nav-item">
      <Link className="nav-link" to={ROUTES.SIGN_IN}>SignIn</Link>
    </li>
    <li className="nav-item">
      <Link className="nav-link" to={ROUTES.SIGN_UP}>SignUp</Link>
    </li>
    <li className="nav-item">
      <Link className="nav-link" to={ROUTES.SIGN_IN}>Post Your Project</Link>
    </li>
  </ul>
);

export default Navigation;
