import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import { withFirebase } from '../Firebase';
import * as ROUTES from '../../constants/routes';
import * as ROLES from '../../constants/roles';

const SignUpPage = () => (
  <SignUpForm />
);

const INITIAL_STATE = {
  firstName : '',
  lastName : '',
  email: '',
  passwordOne: '',
  passwordTwo: '',
  mobileNumber: '',
  address : '',
  city : '',
  country : '',
  isRole : '',
  companyName: '',
  yourTitle:'',
  typeOfIndustry:'',
  companyOverview:'',
  services : [],
  availability :'',
  skills : [],
  education :'',
  experience :'',
  professionalOverview : '',
  error: null,
};

const ERROR_CODE_ACCOUNT_EXISTS = 'auth/email-already-in-use';

const ERROR_MSG_ACCOUNT_EXISTS = `
  An account with this E-Mail address already exists.
  Try to login with this account instead. If you think the
  account is already used from one of the social logins, try
  to sign in with one of them. Afterward, associate your accounts
  on your personal account page.
`;

class SignUpFormBase extends Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }

  onSubmit = event => {
    // const { username, email, passwordOne, isAdmin } = this.state;
    // const roles = [];

    // if (isAdmin) {
    //   roles.push(ROLES.ADMIN);
    // }

    // this.props.firebase
    //   .doCreateUserWithEmailAndPassword(email, passwordOne)
    //   .then(authUser => {
    //     // Create a user in your Firebase realtime database
    //     return this.props.firebase.user(authUser.user.uid).set({
    //       username,
    //       email,
    //       roles,
    //     });
    //   })
    //   .then(() => {
    //     return this.props.firebase.doSendEmailVerification();
    //   })
    //   .then(() => {
    //     this.setState({ ...INITIAL_STATE });
    //     this.props.history.push(ROUTES.HOME);
    //   })
    //   .catch(error => {
    //     if (error.code === ERROR_CODE_ACCOUNT_EXISTS) {
    //       error.message = ERROR_MSG_ACCOUNT_EXISTS;
    //     }

    //     this.setState({ error });
    //   });

    // event.preventDefault();
  };

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  onChangeRadio = event => {
    if(event.target.name == "role"){
      this.setState({ isRole : event.target.value });
    }
  };

  render() {
    const {
      email,
      passwordOne,
      passwordTwo,
      isAdmin,
      error,
    } = this.state;

    const isInvalid =
      passwordOne !== passwordTwo ||
      passwordOne === '' ||
      email === '';

    return (
      <main className="main">
        <div className="signup py-5">
          <div className="container">
            <div className="text-center mb-3">
              <h2 className="display-4 font-weight-bold">SIGN UP</h2>
            </div>
            <form className="needs-validation signup-formbox">
              <div className="row">
              <div className="col-md-12">
                <span className="text-black-50 font65">* MANDATORY FIELDS</span> 
              </div>
              <div className="col-md-6">
                <div className="input-group mb-3">
                  <div className="input-group-prepend"> 
                  <span className="input-group-text" id="name">
                  <i className="fas fa-user"></i></span> 
                  </div>
                  <input type="text" className="form-control" placeholder="First Name"/>
                  <input type="text" className="form-control" placeholder="Last Name"/>
                </div>
              </div>
              <div className="col-md-6">
                <div className="input-group mb-3">
                  <div className="input-group-prepend"> 
                  <span className="input-group-text" id="email">
                  <i className="fas fa-envelope"></i>
                  </span> 
                  </div>
                  <input type="email" className="form-control" placeholder="Email"/>
                </div>
              </div>
              <div className="col-md-6">
                <div className="input-group mb-3">
                  <div className="input-group-prepend"> 
                  <span className="input-group-text" id="password"><i className="fas fa-key"></i></span> 
                  </div>
                  <input type="password" className="form-control" placeholder="Password"/>
                </div>
              </div>
              <div className="col-md-6">
                <div className="input-group mb-3">
                  <div className="input-group-prepend"> 
                  <span className="input-group-text" id="confirmPassword"><i className="fas fa-key"></i></span> 
                  </div>
                  <input type="password" className="form-control" placeholder="Confirm Password"/>
                </div>
              </div>
              <div className="col-md-6">
                <div className="input-group mb-3">
                  <div className="input-group-prepend"> 
                  <span className="input-group-text" id="mobileNumber">
                  <i className="fas fa-phone"></i></span> 
                  </div>
                  <input type="password" className="form-control" placeholder="Mobile Number"/>
                </div>
              </div>
              <div className="col-md-6">
                <div className="input-group mb-3">
                  <div className="input-group-prepend"> <span className="input-group-text"><i className="fas fa-search-location"></i></span> </div>
                  <textarea className="form-control" placeholder="Address" rows="1"></textarea>
                </div>
              </div>
              <div className="col-md-6">
                <div className="input-group mb-3">
                  <div className="input-group-prepend"> <span className="input-group-text" id="city"><i className="fas fa-map-marked-alt"></i></span> </div>
                  <input type="text" className="form-control" placeholder="City"/>
                </div>
              </div>
              <div className="col-md-6">
                <div className="input-group mb-3">
                  <div className="input-group-prepend"> <span className="input-group-text" id="country"><i className="fas fa-globe"></i></span> </div>
                  <input type="text" className="form-control" placeholder="Country" aria-label="Username" aria-describedby="phone"/>
                </div>
              </div>
              <div className="col-md-12">
                <div className="form-group">
                  <label htmlFor="disabledTextInput">I am a</label>
                  <div className="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="businessRadio" name="role" className="custom-control-input business-radio" onChange={this.onChangeRadio.bind(this)} value={ROLES.BUSINESS}/>
                    <label className="custom-control-label" htmlFor="businessRadio"> Business Owner</label>
                  </div>
                  <div className="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="freelancerRadi" name="role" className="custom-control-input business-radio" onChange={this.onChangeRadio.bind(this)} value={ROLES.FREELANCER}/>
                    <label className="custom-control-label" htmlFor="freelancerRadi"> Freelancer</label>
                  </div>
                </div>
              </div>
              { (this.state.isRole == ROLES.BUSINESS) &&
              <div className="col-md-12">
              <div className="">
                <div className="row">
                  <div className="col-md-6">
                    <div className="row">
                      <div className="col-md-12">
                        <div className="input-group mb-3">
                          <div className="input-group-prepend"> 
                          <span className="input-group-text" id="companyName"><i className="fas fa-building"></i></span> </div>
                          <input type="text" className="form-control" placeholder="Company Name" aria-label="Company Name"/>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="input-group mb-3">
                          <div className="input-group-prepend"> <span className="input-group-text" id="yourTitle"><i className="fas fa-user-tie"></i></span> </div>
                          <input type="text" className="form-control" placeholder="Your Title" aria-label="Your Title"/>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="input-group mb-3">
                          <div className="input-group-prepend"> <span className="input-group-text" id="typeOfIndustry"><i className="fas fa-user-tie"></i></span> </div>
                          <select className="custom-select">
                            <option value="">Type of Industry</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    </div>
                    <div className="col-md-6">
                      <div className="input-group mb-3">
                        <div className="input-group-prepend"> <span className="input-group-text"><i className="fas fa-search-location"></i></span> </div>
                        <textarea rows="7" className="form-control" placeholder="Company Overview"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              }
              { (this.state.isRole == ROLES.FREELANCER) &&
              <div className="col-md-12">
                <div className="">
                  <div className="row">
                    <div className="col-md-6">
                      <div className="input-group mb-3">
                        <div className="input-group-prepend"> <span className="input-group-text" id="typeOfIndustry"><i className="fas fa-user-tie"></i></span> </div>
                        <select className="custom-select" >
                          <option value="">Service that you can provide</option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                      </div>
                      <div className="input-group mb-3">
                        <div className="input-group-prepend"> <span className="input-group-text" id="typeOfIndustry"><i className="fas fa-user-tie"></i></span> </div>
                        <select className="custom-select">
                          <option value="">Your skills</option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                      </div>
                      <div className="input-group mb-3">
                        <div className="input-group-prepend"> <span className="input-group-text" id="typeOfIndustry"><i className="fas fa-user-tie"></i></span> </div>
                        <select className="custom-select" required aria-describedby="typeOfIndustry">
                          <option value="">Education</option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                      </div>
                    <div className="input-group mb-3">
                        <div className="input-group-prepend"> <span className="input-group-text" id="typeOfIndustry"><i className="fas fa-user-tie"></i></span> </div>
                        <select className="custom-select">
                          <option value="">Experience Level</option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="input-group mb-3">
                        <div className="input-group-prepend"> <span className="input-group-text" id="typeOfIndustry"><i className="fas fa-user-tie"></i></span> </div>
                        <select className="custom-select">
                          <option value="">Availability</option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                      </div>
                      <div className="input-group mb-3">
                        <div className="input-group-prepend"> <span className="input-group-text"><i className="fas fa-search-location"></i></span> </div>
                        <textarea rows="7" className="form-control" placeholder="Write a professional overview"></textarea>
                      </div>
                    </div>
              
                  </div>
                </div>
              </div>
              }
              <div className="col-md-12">
                <div className="text-center">
                <div className="input-group text-right">
                  <button className="btn btn-warning">Submit</button>
                </div>
                </div>
              </div>
              </div>
            </form>
          </div>
        </div>
      </main>

    );
  }
}

const SignUpLink = () => (
  <p>
    Don't have an account? <Link to={ROUTES.SIGN_UP}>Sign Up</Link>
  </p>
);

const SignUpForm = compose(
  withRouter,
  withFirebase,
)(SignUpFormBase);

export default SignUpPage;

export { SignUpForm, SignUpLink };
