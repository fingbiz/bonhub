import React from 'react';

const Landing = () => (
 <main className="main">
  <div className="hero-slider">
    <div className="container">
      <div className="row">
        <div className="col-lg-6 align-self-center">
          <div className="sliderBox">
            <h1>We bridge the gap between <span>freelance digital marketers</span> and <span>small businesses</span></h1>
            <p>Quality Assurance<span > | </span>Project Management<span > | </span>On-time Delivery<span> | </span>On-demand Services<span> | </span> Pay-as-you-go</p>
            <h3>Can't afford an agency / retainer?</h3>
            <p><a className="btn btn-warning text-white">Post Your Digital Marketing</a></p>
          </div>
        </div>
        <div className="col-lg-6"> <img src="images/hero-slider.jpg" alt="" className="img-fluid" /> </div>
      </div>
    </div>
  </div>
  <div className="divider"> 
  <img src="images/divider-1.png" alt="" className="img-fluid"/> </div>
  <div className="love-bonhub text-center">
    <div className="container">
      <h2>Why small businesses and freelancers love bonhub?</h2>
      <p>We could go on and on, but mainly because we bring talented freelancers in the digital marketing industry together. Our freelance-partners are qualified, certified and graded for their skills. We make sure they receive clear work specifications, minimum iteration and on-time payment.</p>
      <p>When it comes to our customers, we handle everything from scope definition to delivery. This means small businesses are guarenteed that the project is well managed, delivered on time and on budget. What's not to love!</p>
    </div>
  </div>
  <div className="worksDone text-center text-white">
    <div className="container">
      <div className="row">
        <div className="col-md-4 my-2"> <img src="images/ico-o1.png" alt=""/>
          <div className="display-4">750+</div>
          <p>Designs</p>
        </div>
        <div className="col-md-4 my-2"> <img src="images/ico-o2.png" alt=""/>
          <div className="display-4">400+</div>
          <p>Ad Spend (Rs.L)</p>
        </div>
        <div className="col-md-4 my-2"> <img src="images/ico-o3.png" alt=""/>
          <div className="display-4">45+</div>
          <p>MarTech Solutions</p>
        </div>
      </div>
    </div>
  </div>
  <div className="what-you-looking text-center">
    <div className="container">
      <h2>What are you looking for?</h2>
      <div className="row">
        <div className="col-md-4 my-2">
          <div className="shadow-box"> <img src="images/Design_icon.png" alt=""/>
            <p>Design Services</p>
          </div>
        </div>
        <div className="col-md-4 my-2">
          <div className="shadow-box"> <img src="images/Advertising_icon.png" alt=""/>
            <p>Online Advertising</p>
          </div>
        </div>
        <div className="col-md-4 my-2">
          <div className="shadow-box"> <img src="images/marktech_icon.png" alt=""/>
            <p>Marketing Tech Services</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div className="how-it-works text-center bg-light">
    <div className="container">
      <h2>How it works for small business startups?</h2>
      <div className="row mt-5">
        <div className="col-md-3"> <img src="images/1_Howitwork_payicon.png" alt=""/>
          <h5>Register</h5>
          <p>Share your business credentials and register</p>
        </div>
        <div className="col-md-3"> 
        <img src="images/2_Howitwork_postprojecticon.png" alt=""/>
          <h5>Talk to our PM</h5>
          <p>Discuss your project requirements</p>
        </div>
        <div className="col-md-3"> 
        <img src="images/3_Howitwork_registericon.png" alt=""/>
          <h5>Agree TAT & Pay</h5>
          <p>Agree on timelines, scope and pay online</p>
        </div>
        <div className="col-md-3"> 
        <img src="images/4_Howitwork_sign-offprojecticon.png" alt=""/>
          <h5>Sign-off project</h5>
          <p>Receive final work product and rate the service</p>
        </div>
      </div>
    </div>
  </div>
</main>
);

export default Landing;
